package com.xjj.sso.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xjj.sso.client.SSOConstants;
/**
 * 该地址下面都是受保护的资源
 * @author zhanghejie
 */
@Controller
@RequestMapping("/auth")
public class AuthController {

	@RequestMapping("/home")
    public String home(Model model) {
		
		model.addAttribute("sso_server_logout",SSOConstants.SSO_SERVER_SSOLOGOUTURL);
        return "/auth/home";
    }
}

<#include "/templates/xjj-index.ftl">
<#--导航-->
<@navList navs=navArr/>

<script>
    	function clearKey(){ 
    		var key = $("#key").val();
    		
    		$.ajax({
			   type: "POST",
			   url: "${base}/xsso/memcache/delete",
			   data: "key="+key,
			   success: function(msg){
			    	XJJ.msg(msg.message);
			   }
			});

        }
        
        
        function getKey(){ 
    		var key = $("#getKey").val();
    		$.ajax({
			   type: "POST",
			   url: "${base}/sso/memcache/get",
			   data: "key="+key,
			   success: function(user){
			   	if(null != user && "null"!=user && ""!=user)
			   	{
			   		$("#email").html(user.email);
			   		$("#fromProject").html(user.fromProject);
			   		$("#loginName").html(user.loginName);
			   		$("#mobile").html(user.mobile);
			   		$("#orgCode").html(user.orgCode);
			   		$("#orgName").html(user.orgName);
			   		$("#password").html(user.password);
			   		$("#projectUserId").html(user.projectUserId);
			   		$("#status").html(user.status);
			   		$("#userId").html(user.userId);
			   		$("#userName").html(user.userName);
			   		$("#userType").html(user.userType);
					
					XJJ.msg("查询成功");
			   	}else
			   	{
			   		$("#email").html("");
			   		$("#fromProject").html("");
			   		$("#loginName").html("");
			   		$("#mobile").html("");
			   		$("#orgCode").html("");
			   		$("#orgName").html("");
			   		$("#password").html("");
			   		$("#projectUserId").html("");
			   		$("#status").html("");
			   		$("#userId").html("");
			   		$("#userName").html("");
			   		$("#userType").html("");
			   		XJJ.msg("不存在缓存");
			   	}
			   }
			});

        }
    </script>

  KEY: <input id="key" type="text" name="key" style="width:300px;"/> <@button icon="trash" type="primary" onclick="clearKey();">清除KEY</@button></br>
  KEY: <input id="getKey" type="text" style="width:300px;"/> <@button icon="eye-open" type="primary" onclick="getKey();">查看KEY</@button></br>
  
  </br>
  
  <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">缓存信息</h3>
      </div>
      <div class="panel-body">
        
        
        
          <div class="profile-user-info profile-user-info-striped">
          
			<div class="profile-info-row">
				<div class="profile-info-name"> email </div>
		
				<div class="profile-info-value">	
					<span class="editable editable-click" id="email"></span>
				</div>
			</div>
			
			<div class="profile-info-row">
				<div class="profile-info-name"> fromProject </div>
		
				<div class="profile-info-value">	
					<span class="editable editable-click" id="fromProject"></span>
				</div>
			</div>
			<div class="profile-info-row">
				<div class="profile-info-name"> loginName </div>
		
				<div class="profile-info-value">	
					<span class="editable editable-click" id="loginName"></span>
				</div>
			</div>
			<div class="profile-info-row">
				<div class="profile-info-name"> orgCode </div>
		
				<div class="profile-info-value">	
					<span class="editable editable-click" id="orgCode"></span>
				</div>
			</div>
			

			<div class="profile-info-row">
				<div class="profile-info-name"> orgName </div>
		
				<div class="profile-info-value">	
					<span class="editable editable-click" id="orgName"></span>
				</div>
			</div>
			<div class="profile-info-row">
				<div class="profile-info-name"> password </div>
		
				<div class="profile-info-value">	
					<span class="editable editable-click" id="password"></span>
				</div>
			</div>
			<div class="profile-info-row">
				<div class="profile-info-name"> projectUserId </div>
		
				<div class="profile-info-value">	
					<span class="editable editable-click" id="projectUserId"></span>
				</div>
			</div>
			<div class="profile-info-row">
				<div class="profile-info-name"> userId </div>
		
				<div class="profile-info-value">	
					<span class="editable editable-click" id="userId"></span>
				</div>
			</div>
			<div class="profile-info-row">
				<div class="profile-info-name"> userName </div>
		
				<div class="profile-info-value">	
					<span class="editable editable-click" id="userName"></span>
				</div>
			</div>
			<div class="profile-info-row">
				<div class="profile-info-name"> userType </div>
		
				<div class="profile-info-value">	
					<span class="editable editable-click" id="userType"></span>
				</div>
			</div>
		  </div>
        
      </div>
    </div>
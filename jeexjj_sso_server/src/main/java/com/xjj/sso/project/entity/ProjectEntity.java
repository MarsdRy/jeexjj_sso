/****************************************************
 * Description: Entity for t_sso_project
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-05-09 zhanghejie Create File
**************************************************/

package com.xjj.sso.project.entity;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.xjj.framework.entity.EntitySupport;

public class ProjectEntity extends EntitySupport {

    private static final long serialVersionUID = 1L;
    public ProjectEntity(){}
    private String name;//NAME
    private String code;//CODE
    private Long createUserId;//CREATE_USER_ID
    private String createUserName;//CREATE_USER_NAME
    private Date createDate;//CREATE_DATE
    private String loginUrl;//LOGIN_URL
    private String validIp;//VALID_IP
    private String pwdType;//PWD_TYPE
    private String status;//status
    /**
     * 返回NAME
     * @return NAME
     */
    public String getName() {
        return name;
    }
    
    /**
     * 设置NAME
     * @param name NAME
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * 返回CODE
     * @return CODE
     */
    public String getCode() {
        return code;
    }
    
    /**
     * 设置CODE
     * @param code CODE
     */
    public void setCode(String code) {
        this.code = code;
    }
    
    /**
     * 返回CREATE_USER_ID
     * @return CREATE_USER_ID
     */
    public Long getCreateUserId() {
        return createUserId;
    }
    
    /**
     * 设置CREATE_USER_ID
     * @param createUserId CREATE_USER_ID
     */
    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }
    
    /**
     * 返回CREATE_USER_NAME
     * @return CREATE_USER_NAME
     */
    public String getCreateUserName() {
        return createUserName;
    }
    
    /**
     * 设置CREATE_USER_NAME
     * @param createUserName CREATE_USER_NAME
     */
    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }
    
    /**
     * 返回CREATE_DATE
     * @return CREATE_DATE
     */
    public Date getCreateDate() {
        return createDate;
    }
    
    /**
     * 设置CREATE_DATE
     * @param createDate CREATE_DATE
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
    
    /**
     * 返回LOGIN_URL
     * @return LOGIN_URL
     */
    public String getLoginUrl() {
        return loginUrl;
    }
    
    /**
     * 设置LOGIN_URL
     * @param loginUrl LOGIN_URL
     */
    public void setLoginUrl(String loginUrl) {
        this.loginUrl = loginUrl;
    }
    
    /**
     * 返回VALID_IP
     * @return VALID_IP
     */
    public String getValidIp() {
        return validIp;
    }
    
    /**
     * 设置VALID_IP
     * @param validIp VALID_IP
     */
    public void setValidIp(String validIp) {
        this.validIp = validIp;
    }
    
    /**
     * 返回PWD_TYPE
     * @return PWD_TYPE
     */
    public String getPwdType() {
        return pwdType;
    }
    
    /**
     * 设置PWD_TYPE
     * @param pwdType PWD_TYPE
     */
    public void setPwdType(String pwdType) {
        this.pwdType = pwdType;
    }
    
    /**
     * 返回status
     * @return status
     */
    public String getStatus() {
        return status;
    }
    
    /**
     * 设置status
     * @param status status
     */
    public void setStatus(String status) {
        this.status = status;
    }
    

    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append("com.xjj.sso.project.entity.ProjectEntity").append("ID="+this.getId()).toString();
    }
}


package com.xjj.sso.server.pojo;

import java.io.Serializable;

import com.xjj.framework.XJJConstants;
import com.xjj.framework.utils.StringUtils;
/**
 * sso返回的
 * @author zhanghejie
 */
public class SSOIdentity implements Serializable{

	private static final long serialVersionUID = 1L;

	public static enum MessageType {success,error};
	
	private MessageType type = MessageType.success;
	private String message = "";
	private User user;
	
	public SSOIdentity() {
		super();
	}

	/**
	 * @return 消息类型
	 */
	public String getType() {
		switch(type){
			case success:
			return XJJConstants.MSG_TYPE_SUCCESS;
			case error:
				return XJJConstants.MSG_TYPE_ERROR;
		}
		return XJJConstants.MSG_TYPE_SUCCESS;
	}
	/**
	 * 设置消息类型
	 * @param 消息类型
	 */
	public void setType(MessageType type) {
		this.type = type;
	}
	/**
	 * @return 消息内容
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * 设置消息内容
	 * @param 消息内容
	 */
	public void setMessage(Object... message) {
		this.message = StringUtils.join(message);
	}
	
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	/**
	 * 用户验证通过
	 * @param user
	 * @return
	 */
	public static SSOIdentity success(User user){
		SSOIdentity ssoId = new SSOIdentity();
		ssoId.setType(MessageType.success);
		ssoId.setUser(user);
		return ssoId;
	}
	
	/**
	 * 用户验证通过
	 * @param user
	 * @return
	 */
	public static SSOIdentity success(String str){
		SSOIdentity ssoId = new SSOIdentity();
		ssoId.setType(MessageType.success);
		ssoId.setMessage(str);
		return ssoId;
	}

	/**
	 * 用户验证失败
	 * @param user
	 * @return
	 */
	public static SSOIdentity error(Object... message){
		SSOIdentity ssoId = new SSOIdentity();
		ssoId.setType(MessageType.error);
		ssoId.setMessage(message);
		return ssoId;
	}
}
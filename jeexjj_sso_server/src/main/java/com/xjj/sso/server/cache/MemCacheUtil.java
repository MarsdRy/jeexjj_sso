package com.xjj.sso.server.cache;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;

import com.whalin.MemCached.MemCachedClient;
import com.whalin.MemCached.SockIOPool;
import com.xjj.framework.sec.entity.XjjUser;
import com.xjj.sso.server.ticket.GrantingTicket;
import com.xjj.sso.server.util.SSOConfiguration;


/**
 * MemCache
 * 缓存工具类
 * @author zhanghejie
 * @since 2015-3-20
 */

public class MemCacheUtil {

	private static Log log = LogFactory.getLog(MemCacheUtil.class);
	private static MemCachedClient cache = new MemCachedClient();
	private static String PREFIX_GT = "XGT";
	static {
		init();
	}

	private static void init() {

		String serverstr = SSOConfiguration.get("memcache.servers");

		String[] serverArray = serverstr.split(",");

		SockIOPool pool = SockIOPool.getInstance();

		pool.setServers(serverArray);

		pool.setFailover(true);

		pool.setInitConn(10);

		pool.setMinConn(10);

		pool.setMaxConn(500);

		pool.setMaintSleep(30);

		pool.setNagle(false);

		// 连接建立后对超时的控制
		pool.setSocketTO(3000);
		// 连接建立时对超时的控制
		pool.setSocketConnectTO(3000);

		pool.setMaxBusyTime(1000 * 10);

		// initialize the connection pool
		// 初始化一些值并与MemcachedServer段建立连接
		pool.initialize();

	}	
	/**
	 清除缓存
	 * @return
	 */

	public static void removeAll() {

		try {
			cache.flushAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void remove(String key) {

		log.info("MemCacheUtil.remove key = " + key+"===value="+cache.get(key));
		try {
			cache.delete(key);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * 清除该用户的memcache缓存
	 * @param user
	 */
	public static void remove(XjjUser user) {

		if(null==user)
		{
			return;
		}
		if(null != user.getLoginName())
		{
			remove(user.getLoginName());
		}
		if(null != user.getEmail())
		{
			remove(user.getEmail());
		}
		if(null != user.getMobile())
		{
			remove(user.getMobile());
		}
	}

	public static Object get(String key) {
		log.info("MemCacheUtil.get key = " + key+"===value="+key);
		try {

			Object obj = cache.get(key);
			return obj;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public static Map<String, Object> getMulti(String[] keys) {
		try {
			return cache.getMulti(keys);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 
	 * @param key
	 * @param value
	 */

	public static void add(String key, Object value) {

		try {
			cache.set(key, value);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * @param key
	 * @param value
	 * @param expiry
	 */
	public static void put(String key, Object value, int hour) {

		Date date = new Date(); 
		Calendar cal = Calendar.getInstance(); 
		cal.setTime(date); 
		cal.add(Calendar.HOUR,hour);
		try {
			cache.set(key, value, cal.getTime());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static List<GrantingTicket> getAllGrantingTicket() {
		List<GrantingTicket> gtList = new ArrayList<GrantingTicket>();
		String hosts = SSOConfiguration.get("memcache.servers");
		
		String host = hosts.split("\\:")[0];
		int port = 11211;
		try{
			port =  Integer.parseInt(hosts.split("\\:")[1]);
			
		}catch(Exception e)
		{
			port = 11211;
		}
		
		List<String> keyList = allkeys(host,port);
		for (int i = 0; i < keyList.size(); i++) {
			
			if(null !=keyList.get(i) && null!=(GrantingTicket)cache.get(keyList.get(i)))
			{
				gtList.add((GrantingTicket)cache.get(keyList.get(i)));
			}
		}
		
		return gtList;
	}
	
	
	public static List<String> allkeys(String host, int port){
        StringBuffer r = new StringBuffer();
        List<String> keyList = new ArrayList<String>();
        
        try {
            Socket socket = new Socket(host, port);
            PrintWriter os = new PrintWriter(socket.getOutputStream());
            BufferedReader is = new BufferedReader(new InputStreamReader( socket.getInputStream()));
            os.println("stats items");
            os.flush();
            String l ;
            while (!(l = is.readLine()).equals("END")) {
                r.append(l).append("\n");
            }
            String rr = r.toString();
            Set<String> ids = new HashSet<String>();
            if(rr.length() > 0){
                r = new StringBuffer();//items
                rr.replace("STAT items", "");
                for(String s : rr.split("\n")){
                    ids.add(s.split(":")[1]);
                }
                
                String gtStr = null;
                if (ids.size() > 0){
                    r = new StringBuffer();//
                    for(String s : ids){
                        os.println("stats cachedump "+ s +" 0");
                        os.flush();
                        while (!(l = is.readLine()).equals("END")) {
                            //r.append(l.split(" ")[1]).append("\n");
                        	gtStr = l.split(" ")[1];
                        	if(null != gtStr && gtStr.startsWith(PREFIX_GT))
                        	{
                        		keyList.add(gtStr);
                        	}
                        }
                    }
                }
            }
            os.close();
            is.close();
            socket.close();
        } catch (Exception e) {
            System.out.println("Error" + e);
        }
        return keyList;
    }
	

	public static void main(String[] args) {

//		List<String> list = new ArrayList<String>();
//		list.add("zhj1");
//		list.add("zhj2");
//		list.add("zhj3");
//		MemCacheUtil.add("kwq1", "kwq1,kwq");
//		MemCacheUtil.add("kwq2", "kwq2,kwq");
//		MemCacheUtil.add("kwq3", "kwq3,kwq");
//		MemCacheUtil.add("list", list);
//		Map<String, Object> map = MemCacheUtil.getMulti(new String[] { "kwq1","kwq2", "kwq3" });
//
//		System.out.println(map);
//		System.out.println(MemCacheUtil.get("list"));
		
		
		List<GrantingTicket> gtList = getAllGrantingTicket();
		System.out.println("==================================================");
		for (int i = 0; i < gtList.size(); i++) {
			if(null != gtList.get(i))
			{
				System.out.println(gtList.get(i));
			}
		}
		
		System.out.println("==================================================");
	}
}